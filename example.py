#grab text of a particular format
import re
#phoneptn=re.compile(",...-....,") #^$ means this is exactly what i want (put inside "^..$")
phoneptn=re.compile(",[0-9]{3,}-[0-9]{4,},")

peopleFH=open("data.txt","r")
outpeopleFH=open("phone.txt","w")#saving the contents to another file

peopleFH.readline() #removes the first line i.e. the column header
for line in peopleFH:
    #fixed field position in the file
    data=line.split(",")
    print(data[2]) #print out that column
    outpeopleFH.write(data[2]+"\n") #write doesnt add the new line by default

print("phones.txt has been created")
outpeopleFH.close()
peopleFH.close()

#phone number could be anywhere one the line
peopleFH=open("data.txt","r")
outpeopleFH=open("phone.txt","w")#saving the contents to another file

peopleFH.readline() #removes the first line i.e. the column header
for line in peopleFH:
    phonenum=phoneptn.search(line) #setting the search
    if phonenum: #if you find the pattern, print out what matches
        try:
            print(phonenum.group().replace(",",""))
            outpeopleFH.write(phonenum.group().replace(",","")+"\n")
        except:
            data=phonenum.group()
            print(data[1].replace(",",""))

outpeopleFH.close()
peopleFH.close()
