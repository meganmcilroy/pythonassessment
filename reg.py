import re #regular expression library

#two ways of looking for data in pyton (searh and match).
#Depends on what youre trying to do
#search - is it present? yes, no?
#match - im interested in what i found. returns array with the values

peopleFH=open("data.txt","r")
ptn=re.compile("^.*ati. ", re.IGNORECASE) #ignores case

for person in peopleFH:
    #if re.search("22",person): #looking for the value '22' in person - similar to grep, boolean return value
    if ptn.search(person):
        print(person.rstrip("\r\n"))

#this brings us back to the begining of the file
peopleFH.seek(0,0)

#creating a variable called matched
for person in peopleFH:
    #matched=re.search("^.*ati. ",person)
    matched=ptn.search(person)
    if matched:
        print("Matched at ",matched.span()) #span tells us the location of where its been found
        print("Words Found: ",matched.group()) #this is what i found
