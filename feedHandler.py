import re
#Searching for the following:
rpre=re.compile("Record Publish")
ttre=re.compile("Type: Trade")
tpre=re.compile("wTradePrice")
tvre=re.compile("wTradeVolume")
rre=re.compile("Regression:")

#opening the file
fh=open("opra_example_regression.log","r")
#sets all the following files to blank
rp=tt=tp=tv=""
for line in fh:
    #removing Regression
    if re.search("Regression:",line):
        line=rre.sub("",line)
    if rpre.search(line):
        rp=line
    if ttre.search(line):
        tt=line
    if rp and tt:
        if tpre.search(line):
            tp=line
        if tvre.search(line):
            tv=line
        if rp and tt and tp and tv:
            print( rp.rstrip( "\r\n" )+tt.rstrip( "\r\n" ) )
            print(tp+tv.rstrip("\r\n"))
            #resetting the variables
            rp=tt=tp=tv=""

#closing the file
fh.close()
