import sys

#len starts from 1 so must always be <2
if len(sys.argv) < 2:
    sys.stderr.write("That's not how you do it\n") #creating an standard error message
    sys.exit(1)

print(sys.argv[1])

x=0
while x <=len(sys.argv):
    try:
        print(sys.argv[x])
        input("See something say nothing: ") #it shows one thing in the array, then enter, then next value etc
        x+=1 #increment through the loop
    except IndexError:
        print("This was your typical developer issue")
        print("But we handled it :-)")
        break
    except KeyboardInterrupt:
        print("Don't do that!")
        break
    except:
        print("Some other error")
        print("Cleaning up and exiting")
        break

print("Successfully reached the end of the problem - and coffee")
