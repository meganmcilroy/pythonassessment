#Python 2
#print "hello world"
#Python 3

import sys #extra stuff people have written to make things easier

print ("hello world")

myname=input("Enter your name:")
print("Hello "+myname)

print("Command line args: "+str(sys.argv)) #prompting the user and showing all the arguements
print(sys.argv[1]) #just the first argument

#Using files
#Regular Expressions (REGEX)
#Loops and Conditions

#For loop and while loops. Better using a for loop for a file as it goes through everything in the file
#"w" will empty the file
#"r" means read
#person is the control variable
#people is the file object
peopleFH=open("data.txt","r")
for person in peopleFH:
    #print (person) - this puts an extra new line in it
    #sys.stdout.write(person) - this takes that extra line out
    #print(person.rstrip('\r\n')) - remove the lines
    fields=person.split(',') #this is an array, used to split using the ,
    fields[-1]=fields[-1].rstrip("\r\n") #strips out the lines in between for better presentation
    #you have to convert different 'types' (int, varchar etc) into a string if you want to concatinte
    #print ("Number of elements: "+str(len(fields))+"\tLast element is: "+str(len(fields)-1))
    #the next two lines do the same thing
    #if "22" in fields:
    if fields[1] == "22":
        print(type(fields[1]))#determines the type of data store in variable
        print("New age: "+str(int(fields[1])+1))
        print("Name: "+fields[0]+"\t"+"Car: "+fields[-1]) #-1 is the last field
    #print ("Name: "+fields[0]+"\t"+"Phone: "+fields[len(fields)-1]) #number of fields in the array. Starts counting at 1

peopleFH.close()
